#!/bin/bash


echo get rtc clock:
echo -n second:
i2cget -f -y 1 0x68 0x00 #Alarm1 seconds
echo -n minute:
i2cget -f -y 1 0x68 0x01 #minutes
echo -n hour  :
i2cget -f -y 1 0x68 0x02 #hours
echo -n day   :
i2cget -f -y 1 0x68 0x03 #days
echo -n date   :
i2cget -f -y 1 0x68 0x04 #days
echo -n month   :
i2cget -f -y 1 0x68 0x05 #days
echo -n year   :
i2cget -f -y 1 0x68 0x06 #days

echo get rtc alarms config
echo -n alarm1 seconds:
i2cget -f -y 1 0x68 0x07 #Alarm1 seconds
echo -n alarm1 minutes:
i2cget -f -y 1 0x68 0x08 #minutes
echo -n alarm1 hours  :
i2cget -f -y 1 0x68 0x09 #hours
echo -n alarm1 days   :
i2cget -f -y 1 0x68 0x0A #days

echo -n alarm2 minutes:
i2cget -f -y 1 0x68 0x0B #Alarm2 min
echo -n alarm2 hours  :
i2cget -f -y 1 0x68 0x0C #hours
echo -n alarm2 days   :
i2cget -f -y 1 0x68 0x0D #days

echo -n control       :
i2cget -f -y 1 0x68 0x0E #control: 0x06 = alarm2, 0x07= alarm1 + alarm2 both activated
echo -n flags         :
i2cget -f -y 1 0x68 0x0F #flags & control
