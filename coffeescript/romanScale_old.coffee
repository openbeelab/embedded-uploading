
sleep = require('../../util/javascript/timeUtils').sleep

_searchEquilibrium = (motor,photoDiode,refValue,options={})->

    if options.debug
        console.log "searching equilibrium..."

    light = photoDiode.getValue(7)
    if options.debug
        console.log "initial light =" + light
    deltaLight = light - refValue

    nbSteps = 0

    if deltaLight > -10
        if options.debug
            console.log "trop loin. je recule..."
        until deltaLight > 10
            motor.backward()
            nbSteps -= 1
            light = photoDiode.getValue(7)
            if options.debug
                console.log("light =" + light)
            deltaLight = light - refValue

        if options.debug
            console.log "marche arriere terminée."

    until deltaLight < 5
        if options.debug
            console.log "j'avance..."
        motor.forward()
        sleep(1025 - deltaLight.abs())
        nbSteps += 1
        light = photoDiode.getValue(7)
        if options.debug
            console.log("light =" + light)
        deltaLight = light - refValue

    return nbSteps/motor.getStepFactor()

module.exports = (scaleConfig,options)->

    stand = scaleConfig.stand
    device = stand.device

    return {

        takeMeasure : ->

            stand.motor.switchOn()
            stand.irDiode.setOn()

            infos = _searchEquilibrium(stand.motor,stand.photoDiode,scaleConfig.refValue,options)

            stand.motor.switchOff()
            stand.irDiode.setOff()

            return infos

        searchEquilibrium : ->

            return @takeMeasure()
    }
