config = require '../../config'
Promise= require 'promise'
getScriptOptions = require '../scriptOptions'
getStand = require '../setupStand'

options = getScriptOptions(config)

standPromise = getStand(options)
standPromise.then (stand) ->

    stand.motor.switchOff()

.catch (err)->
    console.log err
    console.log err.stack
