config = require '../../config'
Promise= require 'promise'
getScriptOptions = require '../scriptOptions'
getStand = require '../setupStand'

options = getScriptOptions(config)

standPromise = getStand(options)
standPromise.then (stand) ->
            
    stand.irDiode.setOn()
    console.log stand.irDiode.pin + " (" + stand.irDiode.getDirection() + ") isOn?: " + stand.irDiode.isOn()

    loop
        light = stand.photoDiode.getValue(7)
        console.log("light =" + light)
.catch (err)->
    console.log err
    console.log err.stack
