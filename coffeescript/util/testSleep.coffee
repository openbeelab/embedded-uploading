sleep = require('../../../util/javascript/timeUtils').sleep
config = require '../../config'
Promise= require 'promise'
getScriptOptions = require '../scriptOptions'
getStand = require '../setupStand'

options = getScriptOptions(config)

standPromise = getStand(options)
standPromise.then (stand) ->
            
    stand.irDiode.setOn()
    console.log stand.irDiode.pin + " (" + stand.irDiode.getDirection() + ") isOn?: " + stand.irDiode.isOn()

    now = new Date()
    console.log now
    10.times ->
        sleep(1)
    console.log(new Date())

.catch (err)->
    console.log err
    console.log err.stack
