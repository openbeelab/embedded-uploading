config = require '../../config'
Promise= require 'promise'
getScriptOptions = require '../scriptOptions'
getStand = require '../setupStand'

options = getScriptOptions(config)

standPromise = getStand(options)
standPromise.then (stand) ->

    stand.motor.switchOn()
    stand.irDiode.setOn()

    #process.on 'SIGINT', () -> #Ctrl-C for stopping the motor

    #    console.log("stopping motor...")
    #    stand.motor.switchOff()
    #    stand.motor.unexportPins()
    #    process.exit()

    if options.debug
        console.log("avance...")
    steps = 0
    loop
        inc = 1
        stand.motor.backward(inc)
        steps += inc
        if options.debug
            light = stand.photoDiode.getValue(7)
            console.log("light =" + light+" ,steps=" + steps)
.catch (err)->
    console.log err
    console.log err.stack
