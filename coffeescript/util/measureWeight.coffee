
config = require './config'
takeMeasure = require './takeMeasure'
saveMeasure = require './saveMeasure'

dbDriver = require '../../util/javascript/dbDriver'
configDb = dbDriver.connectToServer(config).useDb(config.name + "_config")
dataDb = dbDriver.connectToServer(config).useDb(config.name + "_data")

configDb.get config.stand_id
.then (stand) ->

    device = require './devices/' + stand.device
    device.stand = stand

    console.log "device ok"

    for sensor in stand.sensors when sensor.active

        sensor.device = device

        measure = takeMeasure(sensor)
        console.log("measure:" + measure.value)

.catch (err)->

    console.log err
