require('../../../util/javascript/arrayUtils').install()

values = {}

api =

    export : (kernelId)->

        return "ok"

    unexport : (kernelId)->

        return "ok"

    unexportAll : ()->

        return "ok"

    pinsAreUnexported : ()->

        return true

    getDirection : (kernelId) ->

        return "in"

    setDirection : (kernelId,direction)->

        return "ok"

    setInputMode : (kernelId)->

        return "ok"

    setOutputMode : (kernelId)->

        return "ok"

    digitalRead : (pin) ->

        if not values[pin]?
            values[pin] = [on,off].pickRandom()

        return values[pin]

    digitalWrite : (pin,value) ->

        return values[pin] = value

    analogWrite : (pin,value) ->

        return values[pin] = value

    analogRead : (pin,nbValues=1) ->

        if not values[pin]?
            values[pin] = [0..1023].pickRandom()
        values[pin] += [-3..3].pickRandom()
        return values[pin]

module.exports = api
