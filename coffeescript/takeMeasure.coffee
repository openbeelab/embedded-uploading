createMeasureTemplate = require '../../util/javascript/createMeasureTemplate'

module.exports =

    (sensor,options)->

        device = sensor.device
        stand = device.stand

        if device[sensor.process]?

            takeMeasure = device[sensor.process]

        else
            specificProcess = require './' + sensor.name
            takeMeasure = specificProcess(sensor,options)["takeMeasure"]


        measure = createMeasureTemplate(sensor)
        start = (new Date()).valueOf()
        measure.raw_value = takeMeasure(sensor,options)
        measure.timestamp = new Date()
        measure.duration = (measure.timestamp.valueOf() - start)/1000.0
        measure.value = (measure.raw_value - sensor.bias)*sensor.gain
        if options.debug
            console.log "raw value:" + measure.raw_value
            console.log "bias:" + sensor.bias
            console.log "gain:" + sensor.gain
            console.log "value:" + measure.value

        if stand.beehouse_id?
            measure.beehouse_id = stand.beehouse_id

        if sensor.isRelative?
            measure.absoluteSource = null

        return measure
