util = require 'util'
Promise= require 'promise'

module.exports = (options)->

    if options.localConfig
        standPromise = Promise.resolve(options.stand)
        if options.debug
            console.log "using local config file"
    else
        dbDriver = require '../../util/javascript/dbDriver'
        config = options.database
        configDb = dbDriver.connectToServer(config).useDb(config.name + "_config")
        standPromise = configDb.get options.stand._id
        if options.debug
            console.log "using db config"

    standPromise.then (stand)->
    
        if options.mockScale
            stand.device = "mock_sync"

        if options.debug
            console.log "setup stand..."
            console.log "stand:"+util.inspect(stand)

        deviceName = stand.device
        device = require './devices/' + deviceName
        device.name = deviceName
        device.unexportAll()
        device.stand = stand
        stand.device = device

        for own name,sensor of stand.sensors

            if options.debug
                console.log "setup stand for sensor:" + name

            sensor.device = device
            sensor.stand = stand
            sensor.name = name

            if name is "romanScale"

                if options.debug
                    console.log "setup stand for roman scale..."

                scale = sensor
                Pin = require './drivers/pin'
                StepMotor = require('./drivers/stepMotor')

                motorPins = scale.motor
                stand.motor = StepMotor(device,motorPins,options)
                stand.motor.pins = motorPins

                if options.debug
                    console.log "setup irDiode pin"
                irDiodePin = scale.irDiode
                stand.irDiode = Pin.buildGpio(device,irDiodePin,'out',options)
                stand.irDiode.pin = irDiodePin

                if options.debug
                    console.log "setup photodiode pin"
                photoDiodePin = scale.photoDiode
                stand.photoDiode = Pin.buildAdc(device,photoDiodePin,options)
                stand.photoDiode.pin = photoDiodePin

                stand.readArmPosition = ->

                    stand.irDiode.setOn()
                    light = stand.photoDiode.getValue(7)
                    stand.irDiode.setOff()
                    return light

        if options.mockScale
            stand.motor.pulse.listeners.push {on: (event,value)->
                if event is "setValue" and value is on
                    if stand.motor.direction.isOn() then dir = -1 else dir = 1
                    stand.photoDiode.setValue(stand.photoDiode.getValue() + dir*5)
            }
        return stand
    return standPromise
