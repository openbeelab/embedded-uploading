getScriptOptions = require './scriptOptions'
getStand = require './setupStand'

config = require '../config'
options = getScriptOptions(config)
console.log options

standPromise = getStand(config)
standPromise.then (stand) ->

    if options.debug
        console.log stand

    for sensorName,sensor of stand.sensors when (((options?.sensors?.length > 0) and (options.sensors.contains(sensorName))) or (options.sensors is undefined)) and (sensor.active or sensor.isActive)

        sensor.name = sensorName

        if options.debug
            console.log "using sensor:" + sensor.name

        takeMeasure = require './takeMeasure'
        measure = takeMeasure(sensor,options)
        if options.debug
            console.log measure

        if options.saveMeasures
            if options.debug
                console.log "storing measure..."

            dbDriver = require '../../util/javascript/dbDriver'
            dataDb = dbDriver.connectToServer(config.database).useDb(config.database.name + "_data")
            
            saveMeasure = require '../../util/javascript/saveMeasure'
            saveMeasure(measure,dataDb,false)
            .then (result)->
                console.log result
            .catch (err)->
                console.log "erreur save!"
                console.log err

.catch (err)->

    console.log err
