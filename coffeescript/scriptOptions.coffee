
module.exports = (options)->
    #options = {saveMeasures:true,debug:true,mockScale:false,localConfig:true}
    try
        console.log options
        shellOptions = require('minimist')(process.argv,{default:options,boolean:['saveMeasures','debug','mockScale','localConfig']})
        console.log shellOptions
        for own option,value of shellOptions
            options[option] = value
        if options.sensors?.length > 0
            options.sensors = options.sensors.split(",")
    catch e
        console.log "error while reading options from the script cli"
        console.log e
    if options?.debug
        console.log options
    return options
