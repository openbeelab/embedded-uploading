Pin = require "./drivers/pin"
delay = require '../../util/javascript/timeout-as-promise'
Promise = require 'promise'
createMeasureTemplate = require '../../util/javascript/createMeasureTemplate'
saveMeasure = require '../../util/javascript/saveMeasure'

module.exports = (stand,dataDb)->
    
    weight0_button = Pin.buildGpio(stand.device,stand.weight0_pin)
  
    testButton = ->

        if weight0_button.isOn()
            
            measure = createMeasureTemplate(stand.sensors.globalWeightManual)
            measure.value = 0
            measure.raw_value = 0
            measure.beehouse_id = stand.beehouse_id
            
            return saveMeasure(measure,dataDb,false)
        
        Promise.resolve(false)

    watchButton = ->
   
        testButton()
        .then (res) ->
            if not res?._rev
                delay(100).then ->
                    watchButton()
            return res

    return watchButton
