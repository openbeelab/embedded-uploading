sleep = require('../../../util/javascript/timeUtils').sleep
Pin = require './pin'

module.exports = (device,config,options) ->

    if options.debug
        console.log "setup power pin"
    power = Pin.buildGpio(device,config.power,'out',options) #high pour moteur actif
    if options.debug
        console.log "setup disable pin"
    disable = Pin.buildGpio(device,config.disable,'out',options) #high pour moteur actif
    if options.debug
        console.log "setup ms1 pin"
    ms1 = Pin.buildGpio(device,config.ms1,'out',options) #microstep 1/2 pas
    if options.debug
        console.log "setup ms2 pin"
    ms2 = Pin.buildGpio(device,config.ms2,'out',options) #microstep 1/4 pas
    if options.debug
        console.log "setup ms3 pin"
    ms3 = Pin.buildGpio(device,config.ms3,'out',options) #microstep 1/16 pas si ms1 et ms2
    if options.debug
        console.log "setup pulse pin"
    pulse = Pin.buildGpio(device,config.pulse,'out',options) #impulsions
    if options.debug
        console.log "setup dir pin"
    direction = Pin.buildGpio(device,config.direction,'out',options) #avant/arriere
    if options.debug
        console.log "setup sleep pin"
    sleepPin = Pin.buildGpio(device,config.sleep,'out',options) #logique inversée
    if options.debug
        console.log "setup reset pin"
    reset = Pin.buildGpio(device,config.reset,'out',options) #logique inversée

    ms1.setOff()
    ms2.setOff()
    ms3.setOff()

    return {

        pulse : pulse
        direction : direction

        getStepFactor: ()->
            
            if ms1.isOn() && ms2.isOff()
                return 2
            if ms1.isOff() && ms2.isOn()
                return 4
            if ms1.isOn() && ms2.isOn() && ms3.isOff()
                return 8
            if ms1.isOn() && ms2.isOn() && ms3.isOn()
                return 16
            return 1

        switchOn : ->

            if options?.debug
                console.log "motor switched on"

            power.setOn()
            disable.setOff()
            reset.setOn()
            sleepPin.setOn()

            sleep(1)

        switchOff : ->

            if options?.debug
                console.log "motor switched off"
            
            power.setOff()
            disable.setOn()
            reset.setOff()
            sleepPin.setOff()

        forward  : (nbSteps=1) ->
            @move(nbSteps)
        backward : (nbSteps=1) ->
            @move(-1*nbSteps)
        move : (nbSteps=1) ->

            goForward = nbSteps > 0

            nbSteps = nbSteps.abs()
            if options.debug
                console.log "moving " + nbSteps + " steps " + if goForward then "forward" else "backward"


            direction.setValue(goForward)

            nbSteps.times ->

                pulse.setOn()
                sleep(config.stepDelay) # ms
                pulse.setOff()
                sleep(config.stepDelay)

            return

        unexportPins : ->

            power.unexport()
            disable.unexport()
            ms1.unexport()
            ms2.unexport()
            ms3.unexport()
            pulse.unexport()
            direction.unexport()
            sleepPin.unexport()
            reset.unexport()

    }
