sleep = require('../../../util/javascript/timeUtils').sleep
require('../../../util/javascript/numberUtils').install()
util = require 'util'

module.exports =

    buildGpio : (device,pinName,direction,options) ->

        if options?.debug
            console.log("building gpio pin:" + pinName + ",direction:" + direction)
        
        device.export pinName
        device.setDirection(pinName,direction)

        return {

            listeners : []
            getDirection : ->
                device.getDirection(pinName)
            setInputMode : ->
                device.setInputMode(pinName)
            setOutputMode : ->
                device.setOutputMode(pinName)
            isOn : -> @getValue() is on
            isOff : -> @getValue() is off
            getValue : ->
                value = device.digitalRead(pinName)
                for listener in @listeners
                    listener.on 'getValue',value
                return value
            setValue : (value)->
                for listener in @listeners
                    listener.on 'setValue',value
                device.digitalWrite(pinName,value)
            setOn : -> @setValue(on)
            setOff : -> @setValue(off)
            unexport : -> device.unexport pinName
        }

    buildAdc : (device,pinName,options) ->

        return {

            listeners : []
            getValue : (nbValues=1,sleepTime=10) ->
#                console.log pinName
                _values = []
                nbValues.times ->
                    _values.push device.analogRead(pinName)
                    sleep sleepTime
            #                console.log _values
                value = _values.mediane() #sortComparables()[(_values.length/2).floor()]
                for listener in @listeners
                    listener.on 'getValue',value
                return value
            unexport : -> device.unexport pinName
            setValue : (value)->
                for listener in @listeners
                    listener.on 'setValue',value
             #   console.log pinName
             #   console.log value
                device.analogWrite(pinName,value)
        }
