
sleep = require('../../util/javascript/timeUtils').sleep

_searchEquilibrium = (motor,photoDiode,refValue,options={})->

    if options.debug
        console.log "searching equilibrium..."

    if options.debug
        console.log "initial light =" + light

    if options.debug
        console.log "phase1 : marche arriere..."

    if options.debug
        console.log "rattrapage du jeu..."
    motor.backward(210)
    
    light = photoDiode.getValue(7)
    deltaLight = light - refValue
    nbSteps = 0
    
    until deltaLight > 10
        if options.debug
            console.log "je recule..."
        motor.backward()
        nbSteps -= 1
        light = photoDiode.getValue(7)
        if options.debug
            console.log("light =" + light)
        deltaLight = light - refValue

    if options.debug
        console.log "marche arriere terminée."

    if options.debug
        console.log "phase2 : marche avant..."
    
    if options.debug
        console.log "rattrapage du jeu..."
    motor.forward(210)
    
    until deltaLight < 5
        if options.debug
            console.log "j'avance..."
        motor.forward()
        sleepTime = (513 - deltaLight.abs())/2.0
        sleep(sleepTime)
        nbSteps += 1
        light = photoDiode.getValue(7)
        if options.debug
            console.log("light =" + light)
        deltaLight = light - refValue

    if options.debug
        console.log "marche avant terminée."

    return nbSteps/motor.getStepFactor()

module.exports = (scaleConfig,options)->

    stand = scaleConfig.stand
    device = stand.device

    return {

        takeMeasure : ->

            stand.motor.switchOn()
            stand.irDiode.setOn()

            infos = _searchEquilibrium(stand.motor,stand.photoDiode,scaleConfig.refValue,options)

            stand.motor.switchOff()
            stand.irDiode.setOff()

            return infos

        searchEquilibrium : ->

            return @takeMeasure()
    }
