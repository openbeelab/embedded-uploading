world = require '../../../util/javascript/tests/features/support/world'
world.fixtures = require('../../../util/javascript/fixtures')()
expect = require 'must'
Pin = require "../drivers/pin"
delay = require '../../../util/javascript/timeout-as-promise'

module.exports = () ->

    weight0_button = null

    @Given /^the system is watching the button pin$/, ->
        
        weight0_button = Pin.buildGpio(world.stand.device,world.stand.weight0_pin)
        weight0_button.setOff()
        
        watchButton = require("../watchWeightRefButton")(world.stand,world.dataDb)
        watchButton()

    @When /^the user presses the button$/, ->

        weight0_button.setOn()
        delay(300).then ->
            weight0_button.setOff()

    @Then /^the measure has a value of 0 Kgs$/,->

        world.measure.value.must.be(0)
        world.measure.raw_value.must.be(0)
