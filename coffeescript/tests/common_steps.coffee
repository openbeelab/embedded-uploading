#require('../../../util/javascript/objectUtils').install()
require('../../../util/javascript/arrayUtils').install()
require('../../../util/javascript/numberUtils').install()
require('../../../util/javascript/stringUtils').install()
world = require '../../../util/javascript/tests/features/support/world'
world.fixtures = require('../../../util/javascript/fixtures')()
world.options = {}

expect = require 'must'

setupStand = require('../setupStand')

util = require 'util'
module.exports = () ->

    @Given "an openbeelab stand",  () ->

        world.options.debug = true
        world.configDb.get "_design/stand/_view/all"
        .then (result)->

            stand = result?.rows?[0]?.value
            expect(stand).to.exist()
            stand.device = "mock_sync"
            device = require '../devices/' + stand.device
            setupStand(stand,world.options)
            #console.log util.inspect(stand)
            #console.log util.inspect(stand.photoDiode)
            stand.photoDiode.setValue(450)
            stand.motor.pulse.listeners.push {on: (event,value)->
                if event is "setValue" and value is on
                    if stand.motor.direction.isOn() then dir = -1 else dir = 1
                    stand.photoDiode.setValue(stand.photoDiode.getValue() + dir*5)
                    return
            }
            world.stand = stand

    @Given "the temperature is read on the rtc i2c bus and is passed as a command\-line param to the main script",  () ->

        world.options.temperature = 21

    @When /^the system measures the (.*)$/,  (name) ->

        world.sensor = world.stand.sensors[name]
        takeMeasure = require('../takeMeasure')
        world.measure = takeMeasure(world.sensor,world.options)

    @When /^the system stores the measure$/,  () ->

        saveMeasure = require('../../../util/javascript/saveMeasure')
        saveMeasure(world.measure,world.dataDb,false)

    @Then /^the db (.*) view lists the measure$/, (view) ->

        [context,view] = view.split("/")
        world.dataDb.get("_design/"+context+"/_view/"+view)
        .then (result) =>
            expect(result?.rows?[0]?.id).to.exist()
            world.dataDb.get result?.rows?[0]?.id

        .then (measure)->
            expect(measure).to.exist()
            world.measure = measure

    @Then /^the measure has a raw value, a transformed value and a unit$/, () ->

        expect(world.measure.raw_value).to.exist()
        expect(world.measure.value).to.exist()
        expect(world.measure.unit).to.exist()

    @Then "the measure has a valid iso8601 timestamp",  () ->

        expect(world.measure.timestamp).to.exist()
        world.measure.timestamp.must.be.a.date()
        expect(-> Date.parse(world.measure.timestamp)).to.not.throw()

    @Then "the measure is geolocated",  () ->

        expect(world.measure.location_id).to.exist()

    @Then /^the measure has tracability metadata/,  (table) ->

        for metadata in table.rows()
            expect(world.measure[metadata]).to.exist()

    @Then /^the measure has a valid (.*) id$/,  (context) ->

        expect(world.measure[context+"_id"]).to.exist()


    @Then /^all pins are unexported/,->

        world.stand.device.pinsAreUnexported().must.be.true()

    @When /^i launch the (.*) script located in the util folder$/, (direction)->

        world.initialArmPosition = world.stand.readArmPosition()
        world.stand.motor[direction](100)

    @Then /^the motor has moved towards (.*)$/, (direction)->

        armPosition = world.stand.readArmPosition()
        armPosition.must.not.be.between(world.initialArmPostion - 100,world.initialArmPosition + 100)
