#!/bin/bash

#this script is to be placed in /etc/crontab (and only this one) with the following syntax:
#@reboot root /root/openbeelab/embedded-uploading/main_routine.sh > /root/main_routine.log

echo `date`

#echo measure all openbeelab things...
#you can add arguments to script:
#    --debug for more script logs
#    --mockScale for simulating a real device
#    --saveMeasures=false for not saving measures in the db
#for testing
echo launching main measures script...
node /openbeelab/embedded-uploading/javascript/main.js $@ --temperature=`/openbeelab/embedded-uploading/get_rtc_temperature.sh`

#echo wait 3 minutes before shutdown...
#useful for debug, during this time comment the script in /dev/crontab, see instructions at the beginning of the script
#echo sleeping 3 minutes...
#echo `date`
#sleep 3m

#re-activate rtc alarm to reboot
#echo reseting rtc alarm...
#./reset_rtc.sh 

#echo shutdown...
#echo power off! ...
#poweroff
