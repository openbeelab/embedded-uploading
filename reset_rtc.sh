#!/bin/bash

#re-activate alarm1 and/or alarm2 for next planned reboot
#use setup_rtc.sh for setting/changing the config
#use print_rtc_config.sh for displaying current config
i2cset -f -y 1 0x68 0x0F 0x00  #flags

#echo declare external rtc...
#echo ds1307 0x68 > /sys/class/i2c-adapter/i2c-1/new_device
