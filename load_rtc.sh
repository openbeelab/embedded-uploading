#!/bin/bash

echo "declare external rtc (hardware is ds3231 but ds1307 driver is used)..."
echo ds1307 0x68 > /sys/class/i2c-adapter/i2c-1/new_device
echo "/dev/rtc1 is now usable."
echo "external rtc declared."
