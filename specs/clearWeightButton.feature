Feature: setting the beehouse weight to 0 Kgs
    As a croned shell command line
    I want to watch the press of the hardware button
    And push a measure of 0 Kgs to the local db

    Background:
        Given an instance of couchdb
        Given an openbeelab system
        Given an openbeelab stand
        Given the system has a location
        Given the system is watching the button pin

    Scenario: Pressing the button and storing the measure
         When the user presses the button
         #Then the system stores the measure
         Then the db beehouse/weight view lists the measure 
          And the measure has a valid beehouse id
          And the measure is geolocated
          And the measure has a valid iso8601 timestamp
          And the measure has a raw value, a transformed value and a unit
          And the measure has a value of 0 Kgs
          And the measure has tracability metadata
              |metadata   |
              |stand_id   |
              |device_name|
              |sensor_name|
