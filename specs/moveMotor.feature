Feature: manually move the roman scale motor
    As a user connected to a terminal of the embedded os
    I want to launch a script forever moving the motor forward or backward
    until i quit the script

    Background:
        Given an instance of couchdb
        Given an openbeelab system
        Given an openbeelab stand

    Scenario Outline: manually moving the motor
         When i launch the <direction> script located in the util folder
         Then the motor has moved towards <direction> 
    
        Examples:
            |direction|
            |forward  |
            |backward |
