Feature: measuring sensors feature
    As a croned openbeelab embedded routine
    I want to read sensors on the local stand
    And push it to the local db

    Background:
        Given an instance of couchdb
        Given an openbeelab system
        Given an openbeelab stand
        Given the system has a location
        Given the temperature is read on the rtc i2c bus and is passed as a command-line param to the main script

    Scenario Outline: Reading a sensor
         When the system measures the <sensorName>
         Then the measure has a valid <context> id
          And the measure is geolocated
          And the measure has a valid iso8601 timestamp
          And the measure has a raw value, a transformed value and a unit
          And the measure has tracability metadata
              |metadata   |
              |stand_id   |
              |device_name|
              |sensor_name|
          And all pins are unexported

        Examples:
            |sensorName     |context  |sensorType |
            |rtcTemperature |location |temperature|
            |romanScale     |beehouse |weight     |

    Scenario Outline: Reading a sensor and storing the measure
         When the system measures the <sensorName>
          And the system stores the measure
         Then the db <context>/<sensorType> view lists the measure
          And the measure has a valid <context> id
          And the measure is geolocated
          And the measure has a valid iso8601 timestamp
          And the measure has a raw value, a transformed value and a unit
          And the measure has tracability metadata
              |metadata   |
              |stand_id   |
              |device_name|
              |sensor_name|
          And all pins are unexported

        Examples:
            |sensorName     |context  |sensorType |
            |rtcTemperature |location |temperature|
            |romanScale     |beehouse |weight     |
