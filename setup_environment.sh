#!/bin/bash

# crontab -e
#@reboot /openbeelab/embedded-uploading/load_rtc.sh >> /var/log/openbeelab/load_rtc.log
#@reboot /openbeelab/embedded-uploading/setup_environment.sh >> /var/log/openbeelab/setup_environment.log
#@reboot /openbeelab/embedded-uploading/main_routine.sh --debug --localConfig >> /var/log/openbeelab/main_routine.log

echo -------------------------------------------------------------
echo setup environment for openbeelab stand...
#echo declare external rtc...
#echo `date`
#echo ds1307 0x68 > /sys/class/i2c-adapter/i2c-1/new_device
#echo `date`

#echo printing rtc config...
#./print_rtc_config.sh
#echo rtc config printed.

echo setting system time with ntp if available and updating rtc time with it, or using rtc otherwise...
# set time to rtc1 with ntpd if net access
# set system date with ntpd if net available or set system date with rtc1
(ntpdate pool.ntp.org && hwclock --systohc -f /dev/rtc1) || (hwclock --hctosys -f /dev/rtc1 && echo rtc time used for sys time)
echo date should be ok...
echo `date`

./test_couchdb_is_up.sh localhost
echo -n couchdb is up:
echo $?
echo restart couchDB to fix bad start issues...
couchdb -d > /var/log/couchdb/stdout.log 2> /var/log/couchdb/stderr.log
couchdb -b > /var/log/couchdb/stdout.log 2> /var/log/couchdb/stderr.log
echo couchdb restart finished.
./test_couchdb_is_up.sh localhost
echo -n couchdb is up:
echo $?
echo --------------------------------------------------------------
