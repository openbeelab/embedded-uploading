#! /bin/sh
# /etc/init.d/blah
#

touch /var/lock/openbeelab

case "$1" in
    start)
        /openbeelab/embedded-uploading/load_rtc.sh
        /openbeelab/embedded-uploading/setup_environment.sh
        ;;
    stop)
        /openbeelab/embedded-uploading/reset_rtc.sh
        ;;
    measure)
        /openbeelab/embedded-uploading/main_routine.sh --sensors=$2 --saveMeasures=false --debug
        ;;
    routine)
        /openbeelab/embedded-uploading/main_routine.sh
        /openbeelab/embedded-uploading/reset_rtc.sh
        poweroff
        ;;
    motor)
        node /openbeelab/embedded-uploading/javascript/util/$2.js --debug
        ;;
    status)
        echo not implemented...
        ;;
    *)
        echo "Usage: /etc/init.d/openbeelab {start|stop|measure|routine}"
        exit 1
        ;;
esac

exit 0
